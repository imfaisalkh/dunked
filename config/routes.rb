class SubdomainConstraint
  def self.matches?(request)
    subdomains = %w{ www admin }
    request.subdomain.present? && !subdomains.include?(request.subdomain)
  end
end


Rails.application.routes.draw do

  # Devise authetication routes
  devise_for :users, controllers: { registrations: "registrations", sessions: "sessions" }

  # Routes for tenant subdomains
  constraints SubdomainConstraint do
    
    # subdomain admin area
    namespace :admin do
      get "/" => "dashboard#index"
      resources :pages # /admin/pages/ instead of /pages/
      get     "themes"        =>   "themes#index"
      post    "themes"        =>   "themes#update"
      get     "settings"      =>   "settings#index"
      post    "settings"      =>   "settings#update"
      get     "pricing"       =>   "subscriptions#pricing"
      get     "checkout"      =>   "subscriptions#checkout"
      post    "subscribe"     =>   "subscriptions#subscribe"
      post    "update-card"   =>   "subscriptions#update_card"
      delete  "unsubscribe"   =>   "subscriptions#unsubscribe"
    end

    # subdomain front-end area
    get '/' => 'pages#index', :constraints => SubdomainConstraint # subdomain root
    get '/:slug' => 'pages#show' # subdomain pages

  end

  # Routes for TLD (marketing site)
  root "home#index"

  # Stripe webhooks
  mount StripeEvent::Engine, at: '/stripe/webhooks' # provide a custom path

end
