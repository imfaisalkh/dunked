# Setup stripe API key
Stripe.api_key = STRIPE_SECRET

# Setup stripe web-hooks
StripeEvent.configure do |events|

  #Subscription failed/deleted
  events.subscribe 'customer.subscription.deleted' do |event|

    ap 'customer.subscription.deleted'
    ap event

    #Delete the customer's subscription from User model
    subscription 			              = event.data.object
    stripe_customer_id  	          = subscription.customer

    #Move to user model
    user                			      = User.find_by_stripe_customer_id(stripe_customer_id)
    user.subscription_plan 			    = ''

    #user.customer is unchanged
    user.subscription_active_untill = Time.at(0).to_datetime
    user.save!

  end
   
  #Charge succeeded for subscription
  events.subscribe 'customer.subscription.updated' do |event|

    ap 'customer.subscription.updated'
    ap event

    #Called for any change in the subscription object
    subscription           				    = event.data.object
    stripe_customer_id            		= subscription.customer
    
    user                				      = User.find_by_stripe_customer_id(stripe_customer_id)
    user.subscription_plan 				    = subscription.plan.id
    user.subscription_active_untill   = Time.at(subscription.current_period_end).to_datetime
    #user.customer is unchanged
    user.save!

  end

  # events.subscribe 'charge.failed' do |event|
  #   # Define subscriber behavior based on the event object
  #   event.class       #=> Stripe::Event
  #   event.type        #=> "charge.failed"
  #   event.data.object #=> #<Stripe::Charge:0x3fcb34c115f8>
  # end

  # events.all do |event|
  #   # Handle all event types - logging, etc.
  # end

end