class AddStripeSubscriptionPlanToUsers < ActiveRecord::Migration
  def change
    add_column :users, :stripe_subscription_plan, :string
  end
end
