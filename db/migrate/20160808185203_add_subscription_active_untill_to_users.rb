class AddSubscriptionActiveUntillToUsers < ActiveRecord::Migration
  def change
    add_column :users, :subscription_active_untill, :datetime
  end
end
