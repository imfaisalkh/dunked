class AddStripeDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :stripe_customer_id, :string
    add_column :users, :stripe_subscription_id, :string
    add_column :users, :card_type, :string
    add_column :users, :card_last_digits, :string
    add_column :users, :card_exp_month, :integer
    add_column :users, :card_exp_year, :integer
  end
end
