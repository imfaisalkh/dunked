class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :body
      t.string :slug
      t.string :visibility
      t.string :menu_label
      t.string :redirect_url

      t.timestamps null: false
    end
  end
end
