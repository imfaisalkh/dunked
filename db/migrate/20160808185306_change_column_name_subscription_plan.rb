class ChangeColumnNameSubscriptionPlan < ActiveRecord::Migration
  def change
  	rename_column :users, :stripe_subscription_plan, :subscription_plan
  end
end
