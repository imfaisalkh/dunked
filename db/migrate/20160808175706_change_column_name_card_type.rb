class ChangeColumnNameCardType < ActiveRecord::Migration
  def change
  	rename_column :users, :card_type, :card_brand
  end
end
