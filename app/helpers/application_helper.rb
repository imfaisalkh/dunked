module ApplicationHelper

  def url_for_page page
    path = if page.slug.present?
      page.slug
    else
      "pages/#{page.id}"
    end

    root_path + path
  end

  def card_fields_class
    "hidden" if current_user.card_last_digits?
	end

  # Subscription Plan Conditionals
  def is_plan plan
    current_user.subscription_plan == plan
  end
	
end
