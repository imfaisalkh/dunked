module Admin::SettingsHelper

	def homepage_options
		grouped_options_for_select({
			"Pages" => Page.all.map { |page| [ page.title, url_for_page(page) ] }
		}, Setting.where(key: "homepage").first.value)
	end

end
