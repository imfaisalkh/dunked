module Admin::PagesHelper

	def page_usage_notice
    if is_plan("basic")
			"You're using #{Page.count} pages. You've got #{10 - Page.count} left."
    elsif is_plan("professional")
			"You're using #{Page.count} pages. You've got #{100 - Page.count} left."
    elsif is_plan("agency")
			"You're using #{Page.count} pages. You've no limit."
    end
	end

end
