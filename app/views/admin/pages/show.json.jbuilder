json.extract! @page, :id, :title, :body, :slug, :visibility, :menu_label, :redirect_url, :created_at, :updated_at
