json.array!(@pages) do |page|
  json.extract! page, :id, :title, :body, :slug, :visibility, :menu_label, :redirect_url
  json.url page_url(page, format: :json)
end
