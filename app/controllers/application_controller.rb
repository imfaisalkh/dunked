class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :current_user_subscribed?

  protected

  def configure_permitted_parameters
  	# define custom attributes of 'User' model below to allow them to be saved in DB
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :subdomain])
  end

  def current_user_subscribed?
    user_signed_in? && current_user.is_subscribed?
  end
	
end
