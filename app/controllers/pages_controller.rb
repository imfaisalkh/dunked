class PagesController < ContentController
  
  def index
    # dummy homepage for subdomains untill homepage option is coded
  end


  def show
    @page = Page.where(slug: params[:slug]).first
    render template: template_to_render
  end
  
  protected
  
  def template_to_render
    if template_exists?(@page.slug)
      @page.slug
    else
      "page"
    end
  end
  
end
