class Admin::ThemesController < AdminController
  def index
    @themes = get_themes
  end

  def update
    setting_params.each do |key, value|
  	  if Setting.where(key: key).present?
  		  Setting.where(key: key).first.update_attribute :value, value
  		else
  		  Setting.create(key: key, value: value)
  		end
  	end

  	redirect_to admin_themes_path, notice: "Theme activated."
  end
  
  private
  
  def setting_params
  	params.require(:settings).permit(:current_theme)
  end
  
  def get_themes
    themes_directory = File.join(Rails.root, "app/themes/")
    theme_folders = Dir.entries(themes_directory).select do |item|
      !%w(. ..).include?(item)
    end.map do |folder|
      OpenStruct.new((YAML.load_file File.join(themes_directory, folder, "info.yaml")).merge id: folder)
    end
  end  
  
end
