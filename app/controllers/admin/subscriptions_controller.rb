class Admin::SubscriptionsController < AdminController
  
  # VIEW: Pricing Table
  def pricing
  end

  # VIEW: Payment Form
  def checkout
  end

  # PROCESS: Create a new subscriber
  def subscribe

  	# 1: Create/Retrive customer object from stripe server
    if current_user.stripe_customer_id?
      customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)
    else
      customer = Stripe::Customer.create(email: current_user.email)
    end

    # 2: Create/Retrive subscription object on stripe server (based on above customer)
    if current_user.stripe_subscription_id?
      subscription = customer.subscriptions.retrieve(current_user.stripe_subscription_id)
      subscription.source = params[:stripeToken] if params[:stripeToken]
      subscription.plan = params[:plan]
      subscription.save
    else
      subscription = customer.subscriptions.create(
                        source: params[:stripeToken],
                        plan: params[:plan]
                      )
    end

    
    # 3: Save Customer & Subscription attrs. to our DB
    options = {
      stripe_customer_id: customer[:id],
      stripe_subscription_id: subscription[:id],
      subscription_plan: subscription[:plan][:id],
      subscription_active_untill: Time.at(subscription[:current_period_end]).utc
    }

    # Only update the card on file if we're adding a new one
    options.merge!(
      card_brand: params[:card_brand],
      card_last_digits: params[:card_last_digits],
      card_exp_month: params[:card_exp_month],
      card_exp_year: params[:card_exp_year]
    ) if params[:card_last_digits]

    # Update user values in our DB
    current_user.update(options)

    # Redirect to user's dashboard
		redirect_to admin_path, flash: {notice: "Subscribed to #{params[:plan]} plan sucessfully"}  	

  end

  # PROCESS: Update card in stripe server and user table
  def update_card
    customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)
    subscription = customer.subscriptions.retrieve(current_user.stripe_subscription_id)
    subscription.source = params[:stripeToken]
    subscription.save

    current_user.update(
      card_brand: params[:card_brand],
      card_last_digits: params[:card_last_digits],
      card_exp_month: params[:card_exp_month],
      card_exp_year: params[:card_exp_year]
    )

    redirect_to admin_settings_path, notice: "Successfully updated your card"
  end

  # PROCESS: Cancel user's subscription
  def unsubscribe
    customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)
    customer.subscriptions.retrieve(current_user.stripe_subscription_id).delete
    current_user.update(stripe_subscription_id: nil, subscription_plan: nil, subscription_active_untill: nil)

    redirect_to admin_settings_path, notice: "Your subscription has been canceled."
  end

end
