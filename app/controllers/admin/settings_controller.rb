class Admin::SettingsController < AdminController

  def index
     @settings = {}
     pairs = Setting.pluck(:key, :value)
     pairs.each { |key, value| @settings[key] = value }
     @settings
  end

  def update
  	setting_params.each do |key, value|
  	  if Setting.where(key: key).present?
  		  Setting.where(key: key).first.update_attribute :value, value
  		else
  		  Setting.create(key: key, value: value)
  		end
  	end

  	Rails.application.reload_routes!
  	redirect_to admin_settings_path(@setting), notice: "Settings saved."
  end

  private

  def setting_params
  	params.require(:settings).permit(:homepage, :site_title, :site_desc, :site_url, :current_theme)
  end
  
end
