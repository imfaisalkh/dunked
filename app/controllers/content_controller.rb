class ContentController < ApplicationController
  before_filter :set_theme
  
  def set_theme
    theme = Setting.where(key: "current_theme").first.value
    prepend_view_path "app/themes/#{theme}" # change default view path from 'views' folder to 'themes' folder
    # self.class.layout "../../themes/#{theme}/layout" # change default layout view file from 'views/layouts/application.html.erb' to the layout file in theme's folder
  end
end
