class SessionsController < Devise::SessionsController
  # before_action :check_subdomain

  protected

  def after_sign_in_path_for(resource)
    admin_url(subdomain: resource.subdomain)
  end

	# def check_subdomain(resource)
	# 	unless request.subdomain == resource.subdomain
	# 		redirect_to root_path, alert: "You are not authorized to access that subdomain."
	# 	end
	# end

    
end