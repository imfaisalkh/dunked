class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, request_keys: { subdomain: false }

  after_create :create_tenant
         

  # Intercept Devise authetication method to check for :subdomain together with :email attribute (following this tutorial: https://github.com/plataformatec/devise/wiki/How-to:-Scope-login-to-subdomain)
  def self.find_for_authentication(warden_conditions)
    where(:email => warden_conditions[:email], :subdomain => warden_conditions[:subdomain]).first
  end

  def is_subscribed?
    stripe_subscription_id?
  end

  private

  def create_tenant
  	Apartment::Tenant.create(subdomain)
  	Apartment::Tenant.switch!(subdomain)
  end

end
