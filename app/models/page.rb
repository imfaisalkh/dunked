class Page < ActiveRecord::Base
	validates :title, :slug, :visibility, presence: true
	validates :slug, uniqueness: true
end
