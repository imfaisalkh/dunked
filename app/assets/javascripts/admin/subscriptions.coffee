# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  Stripe.setPublishableKey($("meta[name='stripe-key']").attr("content"))

  # 1: REQUEST SINGLE-USE TOKEN
  $form = $('#payment-form')
  $form.submit (event) ->
    
    if $(".card-fields").hasClass("hidden")
      # Use the default card
      $form.get(0).submit() 
    else
        # Disable the submit button to prevent repeated clicks:
        $form.find('.submit').prop 'disabled', true
        # Request a token from Stripe:
        Stripe.card.createToken $form, stripeResponseHandler
    # Prevent the form from being submitted:
    false

  #  Show/Hide behaviour for old subscriber renewel
  $('.use-different-card').on "click", ->
    $(".card-on-file").hide()
    $(".card-fields").removeClass("hidden")


# 2: SUBMIT FORM TO OUR SERVER
stripeResponseHandler = (status, response) ->
  # Grab the form:
  $form = $('#payment-form')
  if response.error
    # Problem!
    # Show the errors on the form:
    $form.find('.payment-errors').text response.error.message
    $form.find('.submit').prop 'disabled', false
    # Re-enable submission
  else
    # Token was created!
    # Get the token ID:
    token = response.id
    # Insert the token ID into the form so it gets submitted to the server:
    $form.append $('<input type="hidden" name="stripeToken">').val(token)
    # Insert card details into the form for submission
    $form.append $('<input type="hidden" name="card_brand" />').val(response.card.brand)
    $form.append $('<input type="hidden" name="card_last_digits" />').val(response.card.last4)
    $form.append $('<input type="hidden" name="card_exp_month" />').val(response.card.exp_month)
    $form.append $('<input type="hidden" name="card_exp_year" />').val(response.card.exp_year)
    # Submit the form:
    $form.get(0).submit()
  return
