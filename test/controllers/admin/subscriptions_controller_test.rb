require 'test_helper'

class Admin::SubscriptionsControllerTest < ActionController::TestCase
  test "should get pricing" do
    get :pricing
    assert_response :success
  end

  test "should get checkout" do
    get :checkout
    assert_response :success
  end

end
